========================
evers2010
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/evers2010/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/evers2010/0.1.1/

.. image:: https://b326.gitlab.io/evers2010/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/evers2010

.. image:: https://b326.gitlab.io/evers2010/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/evers2010/

.. image:: https://badge.fury.io/py/evers2010.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/evers2010

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/evers2010/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/evers2010/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/evers2010/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/evers2010/commits/main
.. #}

Formalism of leaf gas exchange from Evers 2010

