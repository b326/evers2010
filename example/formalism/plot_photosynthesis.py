"""
An variation with PPFD
======================

Plot evolution of An for increasing values of PAR
"""
import matplotlib.pyplot as plt
import numpy as np

from evers2010.photosynthesis import photo_net

t_leaf = 25
t_atm = 20
rh = 0.7

print(photo_net(t_leaf, 1000, t_atm, rh))
print("An=20 [µmol.m-2.s-1] Yin2009 fig7a")

# ppfds = np.linspace(0, 1500, 100)
# js = [photo_net(t_leaf, ppfd, t_atm, rh) for ppfd in ppfds]
#
# # plot
# fig, axes = plt.subplots(1, 1, figsize=(6, 4), squeeze=False)
# ax = axes[0, 0]
# ax.plot(ppfds, js)
#
# fig.tight_layout()
# plt.show()
