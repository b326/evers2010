"""
J variation with PPFD
=====================

Plot evolution of J for increasing values of PAR
"""
import matplotlib.pyplot as plt
import numpy as np

from evers2010.photosynthesis import electron_transport_rate

jmax25 = 80 * 1.5  # [µmol e-.m-2 leaf.s-1] gs_sim
k2ll = 0.2  # [mol e-.mol-1 photons] Zhu 2018, table S1 with unit correction
convexity = 0.87  # [-] Zhu 2018, table S1

ppfds = np.linspace(0, 1500, 100)
js = [electron_transport_rate(ppfd, jmax25, k2ll, convexity) for ppfd in ppfds]

# plot
fig, axes = plt.subplots(1, 1, figsize=(6, 4), squeeze=False)
ax = axes[0, 0]
ax.plot(ppfds, js)

fig.tight_layout()
plt.show()
