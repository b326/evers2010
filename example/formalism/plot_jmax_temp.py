"""
Jmax variation with temperature
===============================

Plot evolution of Jmax for increasing values of temperature
"""
import matplotlib.pyplot as plt
import numpy as np

from evers2010.photo_params import jmax

jmax25 = 80 * 1.5  # [µmol e-.m-2 leaf.s-1] gs_sim
jmax_act = 63500  # [J.mol-1] Zhu 2018, table S1
jmax_deact = 202900  # [J.mol-1] Zhu 2018, table S1
jmax_s = 650  # [J.K-1.mol-1] Zhu 2018, table S1

temps = np.linspace(0, 50, 100)
jmaxs = [jmax(temp, jmax25, jmax_act, jmax_deact, jmax_s) for temp in temps]

# plot
fig, axes = plt.subplots(1, 1, figsize=(6, 4), squeeze=False)
ax = axes[0, 0]
ax.plot(temps, jmaxs)

fig.tight_layout()
plt.show()
