"""
Plot VPD leaf
=============

Evolution of VPD leaf with rh and temperatures
"""
import matplotlib.pyplot as plt
import numpy as np

from evers2010.weather import vpd_leaf, vpd

a1 = 0.84  # [-] Zhu 2018, table S1
b1 = 0.14  # [kPa-1] Zhu 2018, table S1

rhs = [0.2, 0.6, 0.9]
t_atms = [20, 30]

t_leafs = np.linspace(0, 50, 100)

# plot
fig, axes = plt.subplots(1, 1, figsize=(6, 4), squeeze=False)
ax = axes[0, 0]

for t_atm, ls in zip(t_atms, ['-', '--']):
    for rh in rhs:
        ax.plot(t_leafs, [a1 - b1 * vpd_leaf(t_leaf, t_atm, rh) for t_leaf in t_leafs], ls=ls,
                label=f"{t_atm:.0f} [°C], {rh * 100:.0f} [%]")
        # ax.plot(t_leafs, [vpd(t_leaf, rh) for t_leaf in t_leafs], '+',
        #         label=f"{0:.0f} [°C], {rh * 100:.0f} [%]")

ax.legend(loc='upper left')
ax.set_xlabel("t_leaf [°C]")
ax.set_ylabel("vpd_leaf [kPa]")

fig.tight_layout()
plt.show()
