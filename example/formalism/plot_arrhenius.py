"""
Arrhenius type of variation with temperature
============================================

Plot evolution of anyu variable for increasing values of temperature
"""
import matplotlib.pyplot as plt
import numpy as np

from evers2010.photo_params import arrhenius

rd_eact = 46390  # [J.mol-1] Zhu 2018, table S1
rd25 = 0.7  # [?] TODO

temps = np.linspace(0, 50, 100)
vals = [arrhenius(temp, rd25, rd_eact) for temp in temps]

# plot
fig, axes = plt.subplots(1, 1, figsize=(6, 4), squeeze=False)
ax = axes[0, 0]
ax.plot(temps, vals)

fig.tight_layout()
plt.show()
